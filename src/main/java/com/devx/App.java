package com.devx;

import com.devx.chat.ConsoleChatBot;

import java.io.IOException;

public class App {
    public static void main(String[] args) throws IOException {
        ConsoleChatBot consoleChatBot = new ConsoleChatBot();
        consoleChatBot.startDialog();
    }
}
