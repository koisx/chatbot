package com.devx.chat.util;

import com.devx.chat.entity.OutputQuestion;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.File;
import java.io.IOException;
import java.util.Collections;
import java.util.List;
import java.util.Random;

/**
 * Encapsulates basic operations for handling randomization
 */
public class RandomUtil {

    private static final ObjectMapper mapper = new ObjectMapper();
    private static final String HARDCODED_QUESTIONS_PATH = "D:\\Work\\Personal projects\\ChatBot\\src\\main\\resources\\outputQuestion.json";
    private static List<OutputQuestion> questions;

    static {
        File file = new File(HARDCODED_QUESTIONS_PATH);
        try {
            questions = mapper.readValue(file, new TypeReference<List<OutputQuestion>>() {
            });
        } catch (IOException e) {
            questions = Collections.emptyList();
        }
    }

    public static boolean takeRandomDecision() {
        return Math.random() > 0.5;
    }

    public static boolean takeRandomDecision(double probability) {
        return Math.random() > probability;
    }

    public static String getRandomQuestion() {
        return questions.get(new Random().nextInt(questions.size())).getText();
    }

    public static String getRandomListElement(List<String> list) {
        return list.get(new Random().nextInt(list.size()));
    }
}
