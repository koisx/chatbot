package com.devx.chat.exception;

/**
 * Thrown when communication should be terminated urgently
 */
public class UrgentDialogTerminationException extends RuntimeException {
    public UrgentDialogTerminationException(String message) {
        super(message);
    }
}
