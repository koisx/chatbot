package com.devx.chat.filter;

/**
 * Gets rid of unnecessary words, increasing concentration of keywords
 */
public class KeywordFilter {
    public static boolean filter(String input) {
        return !input.equalsIgnoreCase("how")
                && !input.equalsIgnoreCase("why")
                && !input.equalsIgnoreCase("does")
                && !input.equalsIgnoreCase("do")
                && !input.equalsIgnoreCase("what")
                && !input.equalsIgnoreCase("is")
                && !input.equalsIgnoreCase("are");
    }
}
