package com.devx.chat;

import com.devx.InputPhraseProcessor;
import com.devx.chat.exception.UrgentDialogTerminationException;

import java.io.IOException;
import java.util.Scanner;

/**
 * Console implementation of the Chatbot
 */
public class ConsoleChatBot implements ChatBot {

    private static final String BOT_NAME = "Talky";
    private final Scanner scanner = new Scanner(System.in);
    private final InputPhraseProcessor phraseProcessor = new InputPhraseProcessor();

    public ConsoleChatBot() throws IOException {
    }

    public void startDialog() {
        sayHello();
        communicate();
    }

    private void sayHello() {
        System.out.println("Hello! My name is " + BOT_NAME);
        System.out.println("Let's chat a little bit :)");
    }

    private void communicate() {
        try{
            while (scanner.hasNextLine()) {
                String fromPerson = scanner.nextLine();
                System.out.println(phraseProcessor.reactToUserInput(fromPerson));
            }
        } catch (UrgentDialogTerminationException e) {
            System.out.println(e.getMessage());
        }

    }
}
