package com.devx.chat.validation;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

/**
 * Checks if a given word is a valid English word
 */
public class Dictionary {
    private Set<String> wordsSet;

    public Dictionary() throws IOException {
        Path path = Paths.get("D:\\Work\\Personal projects\\ChatBot\\src\\main\\resources\\words.txt");
        byte[] readBytes = Files.readAllBytes(path);
        String wordListContents = new String(readBytes, StandardCharsets.UTF_8).toLowerCase();
        String[] words = wordListContents.split("\n");
        wordsSet = new HashSet<>();
        Collections.addAll(wordsSet, words);
    }

    public boolean contains(String word) {
        return wordsSet.contains(word.toLowerCase());
    }
}
