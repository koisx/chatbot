package com.devx.chat.entity;

import com.devx.chat.unmarchaller.CustomQuestionDeserializer;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

import java.util.List;

/**
 * Question from user
 */
@JsonDeserialize(using = CustomQuestionDeserializer.class)
public class Question {
    private List<String> questionTokens;

    public Question(List<String> questionTokens) {
        this.questionTokens = questionTokens;
    }

    public Question() {
    }

    public List<String> getQuestionTokens() {
        return questionTokens;
    }

    public void setQuestionTokens(List<String> questionTokens) {
        this.questionTokens = questionTokens;
    }
}
