package com.devx.chat.entity;

/**
 * Question which Talky sends to user
 */
public class OutputQuestion {
    private String text;

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }
}
