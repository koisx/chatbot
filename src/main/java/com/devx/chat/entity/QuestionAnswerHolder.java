package com.devx.chat.entity;

import java.util.List;

/**
 * Wrapper for entity from questionAnswerSet.json array
 */
public class QuestionAnswerHolder {
    List<Question> questions;
    String answer;

    public QuestionAnswerHolder(List<Question> questions, String answer) {
        this.questions = questions;
        this.answer = answer;
    }

    public QuestionAnswerHolder() {
    }

    public List<Question> getQuestions() {
        return questions;
    }

    public void setQuestions(List<Question> questions) {
        this.questions = questions;
    }

    public String getAnswer() {
        return answer;
    }

    public void setAnswer(String answer) {
        this.answer = answer;
    }
}
