package com.devx.chat.unmarchaller;

import com.devx.chat.entity.Question;
import com.devx.chat.filter.KeywordFilter;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.stream.Collectors;

/**
 * Jackson deserializer for splitting question from json into tokens
 */
public class CustomQuestionDeserializer extends JsonDeserializer<Question> {

    public CustomQuestionDeserializer() {
    }

    @Override
    public Question deserialize(JsonParser jsonParser, DeserializationContext deserializationContext) throws IOException, JsonProcessingException {
        QuestionAsString questionAsString = jsonParser.readValueAs(QuestionAsString.class);
        Question question = new Question();
        question.setQuestionTokens(new ArrayList<>(Arrays.stream(questionAsString.getQuestion().split(" "))
                .filter(KeywordFilter::filter)
                .map(String::toLowerCase)
                .collect(Collectors.toList())));
        return question;
    }

    public static class QuestionAsString {
        private String question;

        public String getQuestion() {
            return question;
        }

        public void setQuestion(String question) {
            this.question = question;
        }
    }
}
