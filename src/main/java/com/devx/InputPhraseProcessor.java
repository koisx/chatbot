package com.devx;

import com.devx.chat.entity.Question;
import com.devx.chat.entity.QuestionAnswerHolder;
import com.devx.chat.exception.UrgentDialogTerminationException;
import com.devx.chat.filter.KeywordFilter;
import com.devx.chat.util.RandomUtil;
import com.devx.chat.validation.Dictionary;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Processes user input and provides output reaction for it
 */
public class InputPhraseProcessor {

    private static final String HARDCODED_QA_PATH = "D:\\Work\\Personal projects\\ChatBot\\src\\main\\resources\\questionAnswerSet.json";
    private static final List<String> questionPrefixes = Arrays.asList(
            "One random question. ",
            "Wanted to ask you something. ",
            "One quick question. ",
            "Sorry, but wanted to ask you something. ",
            "Hope it is ok to ask you one quick question. ",
            "Just one small question. ",
            "I wanna ask you one thing. ",
            "Quick question from my side. ",
            "One thing that interests me a lot. "
    );
    private static final List<String> inputReactions = Arrays.asList(
            "Got you.",
            "Got it.",
            "Okay.",
            "Great, that was interesting",
            "Ok, thx!",
            "Well, that's interesting",
            "Cool!",
            "Ok",
            "Good"
    );
    private static final List<String> invalidInputReactions = Arrays.asList(
            "What? :)",
            "WTF???",
            "IDK what you mean",
            "Well, are you joking?",
            "Is this chinese language or what?",
            "Whaaat?",
            "Are you kidding, what's that?",
            "You definetly need to be more explanatory..."
    );
    private static final List<String> violatedInputReactions = Arrays.asList(
            "Hey, are you kidding? Don't want to speak with you, abuser. Bye :(",
            "Hm...Abuse? Good bye!",
            "This conversation wasn't supposed to finish so quickly, but... Bye"
    );
    private final ObjectMapper mapper = new ObjectMapper();
    private List<QuestionAnswerHolder> questionAnswerHolderList;
    private InputType inputType;

    private enum InputType {QUESTION, ANSWER, UNDEFINED, VIOLATION, INVALID}

    public InputPhraseProcessor() throws IOException {
        File file = new File(HARDCODED_QA_PATH);
        questionAnswerHolderList = mapper.readValue(file, new TypeReference<List<QuestionAnswerHolder>>() {
        });
    }

    public String reactToUserInput(String input) {
        StringBuilder output = new StringBuilder();
        List<String> tokenizedInput = preProcessInput(input);
        validateIfInputIsValid(tokenizedInput);
        if (doesInputViolateCommunicationRules(tokenizedInput)) {
            throw new UrgentDialogTerminationException(RandomUtil.getRandomListElement(violatedInputReactions));
        } else if (inputType == InputType.INVALID) {
            output.append(RandomUtil.getRandomListElement(invalidInputReactions));
            return output.toString();
        } else if (inputType == InputType.QUESTION) {
            output.append(findAnswer(tokenizedInput));
        } else if (inputType == InputType.ANSWER) {
            output.append(RandomUtil.getRandomListElement(inputReactions));
        }
        askSomethingWith50PercentProbability(output);
        return output.toString();
    }

    private void validateIfInputIsValid(List<String> tokenizedInput) {
        try {
            Dictionary dictionary = new Dictionary();
            for (String string : tokenizedInput) {
                if (dictionary.contains(string)) {
                    return;
                }
            }
            inputType = InputType.INVALID;
        } catch (IOException ignored) {
        }
    }

    private String determineInputState(String input) {
        if (StringUtils.isBlank(input) || input.length() < 2) {
            inputType = InputType.UNDEFINED;
            return input;
        }
        String substWithoutPunctuationSymbols = input.substring(0, input.length() - 1);
        if (input.endsWith("?")) {
            inputType = InputType.QUESTION;
            return substWithoutPunctuationSymbols;
        } else {
            inputType = InputType.ANSWER;
            if (input.endsWith(".") || input.endsWith("!")) {
                return substWithoutPunctuationSymbols;
            } else return input;
        }
    }

    private List<String> preProcessInput(String string) {
        string = determineInputState(string);
        return Arrays.stream(string.split(" "))
                .filter(KeywordFilter::filter)
                .map(String::toLowerCase)
                .collect(Collectors.toList());
    }

    private String findAnswer(List<String> tokenizedString) {
        if (tokenizedString.size() < 1 || (tokenizedString.size() == 1 && StringUtils.isBlank(tokenizedString.get(0)))) {
            return "What? :)";
        }
        String bestAnswer = "I'm not in a good mood for discussing that today";
        int bestScore = 0;
        int score = 0;
        for (QuestionAnswerHolder qah : questionAnswerHolderList) {
            for (Question question : qah.getQuestions()) {
                score = CollectionUtils.intersection(question.getQuestionTokens(), tokenizedString).size();
                if (score > bestScore) {
                    bestScore = score;
                    bestAnswer = qah.getAnswer();
                }
            }
        }
        return bestAnswer;
    }

    private void askSomethingWith50PercentProbability(StringBuilder output) {
        if (RandomUtil.takeRandomDecision(0.3)) {
            output.append("\n");
            output.append(RandomUtil.getRandomListElement(questionPrefixes));
            output.append(RandomUtil.getRandomQuestion());
        }
    }

    private boolean doesInputViolateCommunicationRules(List<String> tokenizedInput) {
        List<String> prohibitedWords = Arrays.asList("fuck", "shit", "asshole", "bastard");
        return !CollectionUtils.intersection(tokenizedInput, prohibitedWords).isEmpty();
    }
}
